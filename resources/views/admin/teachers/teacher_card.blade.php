<html>

<head>
    <title>{{ $data->teacher_name }} | Teacher Card</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" media="all">
    <style type="text/css">
        @media print {
                @page { margin: 0; }
                body { margin: 3cm; }
                .TeacherProfile{
                    width:400px;
                    height: 400px;
                }
                .heading {
                    /* text-align: center; */
                    /* margin: auto; */
                    font-size: 25px;
                    font-weight: 600;
                }

        }
    </style>
</head>

<body>
    <button class="btn btn-primary btn-print float-right noprint">Print</button>

   <div class="row m-2 border p-2">
       <div class="col-sm-3">
            <div class="TeacherProfile">
                <img src="{{ asset($data->teacher_profile_pic) }}" alt="" width="100%" height="auto">
                <span class="heading">Teacher Id</span>
                <br>
                <span>{{ $data->teacher_id }}</span>
            </div>
       </div>
       <div class="col-sm-9">

       </div>
   </div>


    <script>
        $(document).ready(function () {
            $(".btn-print").on('click', function () {
                window.print();
            });
        });
    </script>
</body>
</html>